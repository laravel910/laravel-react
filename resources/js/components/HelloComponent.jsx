import React from 'react';
import ReactDOM from 'react-dom/client';

export default function HelloComponent() {
    return (
        <h1>Hello word!</h1>
    )
}

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(<HelloComponent />);



